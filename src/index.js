import Aos from 'aos';
import Gumshoe from 'gumshoejs';

import 'aos/dist/aos.css';
require('../js/chart')

Aos.init({
    duration: 600,
    easing: "ease-in-out",
    anchorPlacement: "center-center"
});

const spyNav = new Gumshoe(".navigation ul li a", {
    offset: 80,
});