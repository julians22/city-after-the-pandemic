<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://jpi.or.id/city-after-the-pandemic/">
    <meta property="og:title" content="JPI - Survey: City After The Pandemic">
    <meta property="og:description" content="Berdasarkan hasil survei JPI, Januari 2021 Pandemi Covid-19 dengan segala keresahan yang dihasilkannya mengubah aktivitas masyarakat. Pandemi telah mengubah kegiatan warga kota, membatasi ruang gerak, dan menciptakan jarak dengan sesama. Perubahannya tak bisa dihindari dan berpotensi menjadi permanen meski pandemi berakhir.">
    <meta property="og:image" content="https://jpi.or.id/files/uploads/tmbriset_20210322113117.png">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="https://jpi.or.id/city-after-the-pandemic/">
    <meta property="twitter:title" content="JPI - Survey: City After The Pandemic">
    <meta property="twitter:description" content="Berdasarkan hasil survei JPI, Januari 2021 Pandemi Covid-19 dengan segala keresahan yang dihasilkannya mengubah aktivitas masyarakat. Pandemi telah mengubah kegiatan warga kota, membatasi ruang gerak, dan menciptakan jarak dengan sesama. Perubahannya tak bisa dihindari dan berpotensi menjadi permanen meski pandemi berakhir.">
    <meta property="twitter:image" content="https://jpi.or.id/files/uploads/tmbriset_20210322113117.png">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="assets/images/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="description" content="Berdasarkan hasil survei JPI, Januari 2021 Pandemi Covid-19 dengan segala keresahan yang dihasilkannya mengubah aktivitas masyarakat. Pandemi telah mengubah kegiatan warga kota, membatasi ruang gerak, dan menciptakan jarak dengan sesama. Perubahannya tak bisa dihindari dan berpotensi menjadi permanen meski pandemi berakhir.">
    <meta itemprop="name" content="Survey: City After The Pandemic">
    <meta itemprop="image" content="https://jpi.or.id/files/uploads/tmbriset_20210322113117.png">
    <link rel="shortcut icon" href="https://jpi.or.id/reformasi-pasar-reformasi-kota/assets/images/icons/favicon.png">

    <title>Survey: City After the Pandemic</title>

    <!-- Styles -->
    <link href="resources/sass/app.css" type="text/css" rel="stylesheet" />
    <link href="css/font-circular.css" type="text/css" rel="stylesheet" />
    <link href="dist/tailwind.css" type="text/css" rel="stylesheet" />

    <link href="js/apexchart/apexcharts.css" type="text/css" rel="stylesheet" />

    <script src="js/jquery/jquery.min.js"></script>
    <script src="js/apexchart/apexcharts.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>

<body class="pb-10">

    <div class="w-full p-4 block sm:hidden fixed top-0 z-10 navigation-mobile">
        <div class="flex justify-between items-center">
            <a href="#" id="menu-toggle">
                <span class="w-8 block">
                    <img src="img/menu.svg" alt="" class="w-full">
                </span>
            </a>
            <a href="https://jpi.or.id/" class="block">
                <img src="img/logo_black.png" alt="" class="w-12">
            </a>
        </div>
        <ul class="flex-col justify-items-start nav-mobile-item bg-white rounded-md">
            <li class="text-blue p-2">
                <a href="#profilresponden">PROFIL RESPONDEN</a>
            </li>
            <li class="text-pink p-2">
                <a href="#work">WORK</a>
            </li>
            <li class="text-green p-2">
                <a href="#live">LIVE</a>
            </li>
            <li class="text-violet p-2">
                <a href="#play">PLAY</a>
            </li>
            <li class="p-2">
                <a href="#kerinduan">KERINDUAN DAN KEHAWATIRAN</a>
            </li>
            <li class="p-2">
                <a href="#kesimpulan">KESIMPULAN DAN SARAN</a>
            </li>
        </ul>
    </div>

    <!-- section 1  -->
    <section class="bg-center bg-no-repeat bg-cover w-full h-80 sm:h-85-vh relative hero" style="background-image: url('img/hero_img.jpg');">
        <div class="logo">
            <a href="https://jpi.or.id/" class="block">
                <img src="img/brand.png" alt="" class="w-3/4 sm:w-full ml-auto">
            </a>
        </div>

        <div class="hero-content absolute bottom-10 sm:bottom-16 inset-x-0 block w-2/5 ml-8 mr-auto">
            <img src="img/city_after_the_pandemic.png" alt="" class="w-full">
        </div>
    </section>

    <!-- navigation -->

    <div class="w-full py-2 bg-white navigation z-50 hidden sm:block">
        <ul class="flex uppercase items-center justify-center container mx-auto font-bold text-lg h-16">
            <li class="text-blue group">
                <a href="#profilresponden">Profil Responden</a>
                <span class="h-12 my-auto inset-0 w-0.5 bg-gray-darkend absolute ml-auto"></span>
                <span class="absolute h-0.5 bg-gray-darkend inset-x-0 bottom-1 hidden w-4/5 mx-auto border-active group-hover:block"></span>
            </li>
            <li class="text-pink group">
                <a href="#work">WORK</a>
                <span class="h-12 my-auto inset-0 w-0.5 bg-gray-darkend absolute ml-auto"></span>
                <span class="absolute h-0.5 bg-gray-darkend inset-x-0 bottom-1 hidden w-4/5 mx-auto border-active group-hover:block"></span>
            </li>
            <li class="text-green group">
                <a href="#live">LIVE</a>
                <span class="h-12 my-auto inset-0 w-0.5 bg-gray-darkend absolute ml-auto"></span>
                <span class="absolute h-0.5 bg-gray-darkend inset-x-0 bottom-1 hidden w-4/5 mx-auto border-active group-hover:block"></span>
            </li>
            <li class="text-violet group">
                <a href="#play">PLAY</a>
                <span class="h-12 my-auto inset-0 w-0.5 bg-gray-darkend absolute ml-auto group"></span>
                <span class="absolute h-0.5 bg-gray-darkend inset-x-0 bottom-1 hidden w-4/5 mx-auto border-active group-hover:block"></span>
            </li>
            <li class="group">
                <a href="#kerinduan">KERINDUAN DAN KEHAWATIRAN</a>
                <span class="h-12 my-auto inset-0 w-0.5 bg-gray-darkend absolute ml-auto"></span>
                <span class="absolute h-0.5 bg-gray-darkend inset-x-0 bottom-1 hidden w-4/5 mx-auto border-active group-hover:block"></span>
            </li>
            <li class="group">
                <a href="#kesimpulan">KESIMPULAN DAN SARAN</a>
                <span class="absolute h-0.5 bg-gray-darkend inset-x-0 bottom-1 hidden w-4/5 mx-auto border-active group-hover:block"></span>
            </li>
        </ul>
    </div>

    <!-- section 2 -->

    <section class="container sm:w-1/2 w-auto px-2 sm:px-0 mx-auto py-6" data-aos="fade-in">
        <h4 class="text-lg font-semibold">Berdasarkan hasil survei JPI, Januari 2021</h4>
        <p class="font-semibold mt-2">
            Pandemi Covid-19 dengan segala keresahan yang dihasilkannya mengubah aktivitas masyarakat. Pandemi telah mengubah
            kegiatan warga kota, membatasi ruang gerak, dan menciptakan jarak dengan sesama. Perubahannya tak bisa dihindari dan
            berpotensi menjadi permanen meski pandemi berakhir. <br> <br>

            Jakarta Property Institute menggali potensi perubahan tersebut, terutama yang berkaitan dengan penggunaan ruang. JPI
            menggelar survei online dan dijawab oleh 408 responden yang tersebar di Jabodetabek dan sekitarnya. Survei ini juga
            mengumpulkan ekspektasi/harapan responden ketika pandemi sudah selesai. <br> <br>

            Hasilnya, responden ingin ada perubahan pilihan lokasi bekerja, berbelanja, dan berolahraga saat pandemi berakhir.
            Seberapa besar perbedaan penggunaan ruang nantinya dan apa dampaknya bagi kota?
        </p>
    </section>

    <!-- profile responden -->

    <section class="container sm:w-2/3 w-auto px-2 sm:px-0 mx-auto pb-6 pt-20" id="profilresponden" data-aos="fade-in">
        <div class="content-title-1 border-t-2 border-blue w-full relative">
            <span class="absolute top-0 left-0 ml-2">1.</span>
            <span class="bg-blue text-white py-0.5 px-2 ml-6 inline-block">Profil responden</span>
        </div>

        <div class="content-wrapper-1 mt-2 pl-6">
            <div class="w-full pb-4 relative">
                <h4 class="text-xl font-black leading-none">56,4 %</h4>
                <p class="content__subs">Responden bekerja sebagai karyawan</p>
                <div class="absolute h-px bg-gray-lighten w-80 inset-0 mt-auto">
                    <div class="absolute left-0 sm:left-full bottom-full top-0 sm:-top-2.5 w-full sm:w-max ml-0 sm:ml-3">Jenis Pekerjaan</div>
                </div>
            </div>

            <div class="content-chart-1 flex flex-wrap items-start mt-8 sm:mt-2 sm:ml-80 ml-0">
                <div class="chart-wrapper relative">
                    <div id="chart-1"></div>
                    <span class="block text-center text-blue">Karyawan</span>
                </div>
                <div class="chart-wrapper relative">
                    <div id="chart-2"></div>
                    <span class="block text-center">Pelajar/mahasiswa</span>
                </div>
                <div class="chart-wrapper">
                    <div id="chart-3"></div>
                    <span class="block text-center">Pekerja lepas</span>
                </div>
                <div class="chart-wrapper">
                    <div id="chart-4"></div>
                    <span class="block text-center">Wiraswasta</span>
                </div>
            </div>
        </div>

        <div class="content-wrapper-2 mt-2 pl-6">
            <div class="w-full pb-4 relative">
                <h4 class="text-xl font-black leading-none">67,2 %</h4>
                <p class="content__subs">Responden berusia 20-28 tahun</p>
                <div class="absolute h-px bg-gray-lighten w-80 inset-0 mt-auto">
                    <div class="absolute left-0 sm:left-full bottom-full top-0 sm:-top-2.5 w-full sm:w-max ml-0 sm:ml-3">Usia</div>
                </div>
            </div>

            <div class="content-chart-1 flex mt-4 sm:mt-2 sm:ml-80 ml-0">
                <div class="w-full relative mt-6" id="chart-usia-responden">
                    <div class="w-ful">
                        <img src="img/usia-responden.png" alt="" class="w-full">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- end profile responden -->

    <!-- work responded -->

    <section class="container sm:w-2/3 w-auto px-2 sm:px-0 mx-auto pb-6 pt-20" id="work" data-aos="fade-in">
        <div class="content-title-1 border-t-2 border-pink w-full relative">
            <span class="absolute top-0 left-0 ml-2">2.</span>
            <span class="bg-pink text-white py-0.5 px-2 ml-6 inline-block">Work</span>
        </div>

        <div class="content-wrapper-1 mt-2 pl-6">
            <div class="w-full pb-4 relative">
                <h4 class="text-xl font-black leading-none">65,4 %</h4>
                <p class="content__subs">Responden bekerja di <strong>DKI Jakarta</strong></p>
                <div class="absolute h-px bg-gray-lighten w-80 inset-0 mt-auto">
                    <div class="absolute left-0 sm:left-full bottom-full top-0 sm:-top-2.5 w-full sm:w-max ml-0 sm:ml-3">Lokasi Tempat Kerja</div>
                </div>
            </div>

            <div class="content-chart-1 flex flex-wrap items-start mt-8 sm:mt-2 sm:ml-80 ml-0">
                <div class="chart-wrapper relative">
                    <div id="chart-5"></div>
                    <span class="block text-center text-pink">DKI Jakarta</span>
                </div>
                <div class="chart-wrapper relative">
                    <div id="chart-6"></div>
                    <span class="block text-center">Bodetabek</span>
                </div>
                <div class="chart-wrapper relative">
                    <div id="chart-7"></div>
                    <span class="block text-center">Luar Jabodetabek</span>
                </div>
            </div>
        </div>

        <div class="content-wrapper-2 mt-2 pl-6">
            <div class="w-full pb-4 relative">
                <h4 class="text-xl font-black leading-none">55,2 %</h4>
                <p class="content__subs">Responden mengeluarkan >1-3jt</p>
                <div class="absolute h-px bg-gray-lighten w-80 inset-0 mt-auto">
                    <div class="absolute left-0 sm:left-full bottom-full top-0 sm:-top-2.5 w-full sm:w-max ml-0 sm:ml-3">Pengeluaran per bulan</div>
                </div>
            </div>

            <div class="content-chart-2 flex mt-4 sm:mt-2 sm:ml-80 ml-0">
                <div class="w-full relative mt-6" id="chart-work-responden">
                    <div class="w-full">
                        <img src="img/penghasilan-responden.png" alt="" class="w-full">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="container sm:w-2/3 w-auto px-2 sm:px-0 mx-auto py-6" id="work-2" data-aos="fade-in">
        <div class="content-title-1 border-t-2 border-pink w-full relative">
            <span class="absolute top-0 left-0 ml-2">3.</span>
            <span class="bg-pink text-white py-0.5 px-2 ml-6 inline-block">Work</span>
        </div>

        <div class="content-wrapper-2 mt-2 pl-6">
            <div class="w-full pb-4 relative">
                <h4 class="text-xl font-black leading-none">77,1 %</h4>
                <p class="content__subs">Responden bekerja di kantor sebelum pandemi</p>
                <div class="absolute h-px bg-gray-lighten w-80 inset-0 mt-auto">
                    <div class="absolute left-0 sm:left-full bottom-full top-0 sm:-top-2.5 w-max ml-0 sm:ml-3">Sebelum pandemi</div>
                </div>
            </div>

            <div class="content-chart-2 flex mt-4 sm:mt-2 sm:ml-80 ml-0">
                <div class="w-full relative mt-6" id="chart-work-responden">
                    <div class="w-full">
                        <img src="img/wfo-responden.png" alt="" class="w-full">
                    </div>
                </div>
            </div>
        </div>
        <div class="content-wrapper-2 mt-2 pl-6">
            <div class="w-full pb-4 relative">
                <h4 class="text-xl font-black leading-none">66,3 %</h4>
                <p class="content__subs">Responden bekerja di rumah selama pandemi</p>
                <div class="absolute h-px bg-gray-lighten w-80 inset-0 mt-auto">
                    <div class="absolute left-0 sm:left-full bottom-full top-0 sm:-top-2.5 w-full sm:w-max ml-0 sm:ml-3">Selama pandemi</div>
                </div>
            </div>

            <div class="content-chart-2 flex mt-4 sm:mt-2 sm:ml-80 ml-0">
                <div class="w-full relative mt-6" id="chart-work-responden">
                    <div class="w-full">
                        <img src="img/wfh-responden.png" alt="" class="w-full">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="container sm:w-2/3 w-auto px-2 sm:px-0 mx-auto py-6" id="work-3" data-aos="fade-in">
        <div class="content-title-1 border-t-2 border-pink w-full relative">
            <span class="absolute top-0 left-0 ml-2">4.</span>
            <span class="bg-pink text-white py-0.5 px-2 ml-6 inline-block">Work</span>
        </div>

        <div class="content-wrapper-2 mt-2 pl-6">
            <div class="w-full pb-4 relative">
                <h4 class="text-xl font-black leading-none">42,3 %</h4>
                <p class="content__subs">Responden merasa bekerja di rumah sama efektif dengan di kantor</p>
                <div class="absolute h-px bg-gray-lighten w-80 inset-0 mt-auto">
                    <div class="absolute left-0 sm:left-full bottom-full top-0 sm:-top-2.5 w-full sm:w-max ml-0 sm:ml-3">Apakah pekerjaan menjadi efektif ketika bekerja di rumah?</div>
                </div>
            </div>

            <div class="content-chart-2 flex mt-4 sm:mt-2 sm:ml-80 ml-0">
                <div class="w-full relative mt-6" id="chart-work-responden">
                    <div class="w-full">
                        <img src="img/efektif-responden.png" alt="" class="w-full">
                    </div>
                </div>
            </div>
        </div>
        <div class="content-wrapper-2 mt-2 pl-6">
            <div class="w-full pb-4 relative">
                <h4 class="text-xl font-black leading-none">74,6 %</h4>
                <p class="content__subs">Responden merasa cukup puas dengan kualitas koneksi internet di rumah</p>
                <div class="absolute h-px bg-gray-lighten w-80 inset-0 mt-auto">
                    <div class="absolute left-0 sm:left-full bottom-full top-0 sm:-top-2.5 w-full sm:w-max ml-0 sm:ml-3">Seberapa puas dengan koneksi internet dirumah?</div>
                </div>
            </div>

            <div class="content-chart-2 flex mt-4 sm:mt-2 sm:ml-80 ml-0">
                <div class="w-full relative mt-6" id="chart-work-responden">
                    <div class="w-full">
                        <img src="img/int-responden.png" alt="" class="w-full">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="container sm:w-2/3 w-auto px-2 sm:px-0 mx-auto py-6" id="work-4" data-aos="fade-in">
        <div class="content-title-1 border-t-2 border-pink w-full relative">
            <span class="absolute top-0 left-0 ml-2">5.</span>
            <span class="bg-pink text-white py-0.5 px-2 ml-6 inline-block">Work</span>
        </div>

        <div class="content-wrapper-1 mt-2 pl-6">
            <div class="w-full pb-4 relative">
                <h4 class="text-xl font-black leading-none">55,6 %</h4>
                <p class="content__subs">Responden berharap setelah pandemi bisa bekerja kombinasi di rumah dan di kantor</p>
                <div class="absolute h-px bg-gray-lighten w-80 inset-0 mt-auto">
                    <div class="absolute left-0 sm:left-full bottom-full top-0 sm:-top-2.5 w-full sm:w-max ml-0 sm:ml-3">Setelah pandemi dimana responden ingin bekerja?</div>
                </div>
            </div>

            <div class="content-chart-1 flex flex-wrap items-start mt-8 sm:mt-2 sm:ml-80 ml-0">
                <div class="chart-wrapper relative">
                    <div id="chart-8"></div>
                    <span class="block text-center text-pink bottom-0 inset-x-0">Kombinasi di rumah dan di kantor</span>
                </div>
                <div class="chart-wrapper relative">
                    <div id="chart-9"></div>
                    <span class="block text-center bottom-0 inset-x-0">Total di kantor</span>
                </div>
                <div class="chart-wrapper relative">
                    <div id="chart-10"></div>
                    <span class="block text-center bottom-0 inset-x-0">Total di rumah</span>
                </div>
                <div class="chart-wrapper relative">
                    <div id="chart-11"></div>
                    <span class="block text-center bottom-0 inset-x-0">Tetap di toko/ruko/tempat kerja karena tidak ada pilihan</span>
                </div>
            </div>
        </div>
    </section>

    <section class="container sm:w-2/3 w-auto px-2 sm:px-0 mx-auto py-6" id="work-5" data-aos="fade-in">
        <div class="content-title-1 border-t-2 border-pink w-full relative">
            <span class="absolute top-0 left-0 ml-2">6.</span>
            <span class="bg-pink text-white py-0.5 px-2 ml-6 inline-block">Work</span>
        </div>

        <div class="content-wrapper-2 mt-2 pl-6">
            <div class="w-full pb-4 relative">
                <h4 class="text-xl font-black leading-none">82,2 %</h4>
                <p class="content__subs">Selama pandemi, responden rapat secara online</p>
                <div class="absolute h-px bg-gray-lighten w-80 inset-0 mt-auto">
                    <div class="absolute left-0 sm:left-full bottom-full top-0 sm:-top-2.5 w-full sm:w-max ml-0 sm:ml-3">Selama pandemi</div>
                </div>
            </div>

            <div class="content-chart-2 flex mt-4 sm:mt-2 sm:ml-80 ml-0">
                <div class="w-full relative mt-6" id="chart-work-responden">
                    <div class="w-full">
                        <img src="img/rapatonline-responden.png" alt="" class="w-full">
                    </div>
                </div>
            </div>
        </div>
        <div class="content-wrapper-2 mt-2 pl-6">
            <div class="w-full pb-4 relative">
                <h4 class="text-xl font-black leading-none">60,6 %</h4>
                <p class="content__subs">Setelah pandemi, responden berharap bisa rapat secara tatap muka</p>
                <div class="absolute h-px bg-gray-lighten w-80 inset-0 mt-auto">
                    <div class="absolute left-0 sm:left-full bottom-full top-0 sm:-top-2.5 w-full sm:w-max ml-0 sm:ml-3">Setelah pandemi</div>
                </div>
            </div>

            <div class="content-chart-2 flex mt-4 sm:mt-2 sm:ml-80 ml-0">
                <div class="w-full relative mt-6" id="chart-work-responden">
                    <div class="w-full">
                        <img src="img/rapattatapmuka-responden.png" alt="" class="w-full">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="container sm:w-2/3 w-auto px-2 sm:px-0 mx-auto py-6" id="work-6" data-aos="fade-in">
        <div class="content-title-1 border-t-2 border-pink w-full relative">
            <span class="absolute top-0 left-0 ml-2">7.</span>
            <span class="bg-pink text-white py-0.5 px-2 ml-6 inline-block">Work</span>
        </div>

        <div class="content-wrapper-1 mt-2 pl-6">
            <div class="w-full pb-4 relative">
                <h4 class="text-xl font-black leading-none">69 %</h4>
                <p class="content__subs">Responden memilih transportasi pribadi jika harus kembali ke kantor setelah pandemi berakhir</p>
                <div class="absolute h-px bg-gray-lighten w-80 inset-0 mt-auto">
                    <div class="absolute left-0 sm:left-full bottom-full top-0 sm:-top-2.5 w-full sm:w-max ml-0 sm:ml-3">Pilihan transportasi ke kantor setelah pandemi berakhir</div>
                </div>
            </div>

            <div class="content-chart-1 flex flex-wrap items-start mt-12 sm:mt-2 sm:ml-80 ml-0">
                <div class="chart-wrapper relative">
                    <div id="chart-12"></div>
                    <span class="block text-center bottom-0 inset-x-0 text-pink">Transportasi pribadi</span>
                </div>
                <div class="chart-wrapper relative">
                    <div id="chart-13"></div>
                    <span class="block text-center bottom-0 inset-x-0">Transportasi umum</span>
                </div>
                <div class="chart-wrapper relative">
                    <div id="chart-14"></div>
                    <span class="block text-center bottom-0 inset-x-0">Transportasi online</span>
                </div>
                <div class="chart-wrapper relative">
                    <div id="chart-15"></div>
                    <span class="block text-center bottom-0 inset-x-0">Jalan kaki/bersepeda</span>
                </div>
            </div>
        </div>
    </section>

    <section class="container sm:w-2/3 w-auto px-2 sm:px-0 mx-auto py-6" id="work-7" data-aos="fade-in">
        <div class="content-title-1 border-t-2 border-pink w-full relative">
            <span class="absolute top-0 left-0 ml-2">8.</span>
            <span class="bg-pink text-white py-0.5 px-2 ml-6 inline-block">Work</span>
        </div>

        <div class="content-wrapper-2 mt-2 pl-6">
            <div class="w-full pb-4 relative">
                <h4 class="text-xl font-black leading-none">39,4 %</h4>
                <p class="content__subs">Responden selama pandemi, memilih memasak/makan di rumah ketika jam istirahat</p>
                <div class="absolute h-px bg-gray-lighten w-80 inset-0 mt-auto">
                    <div class="absolute left-0 sm:left-full bottom-full top-0 sm:-top-2.5 w-full sm:w-max ml-0 sm:ml-3">Selama pandemi</div>
                </div>
            </div>

            <div class="content-chart-2 flex mt-2 sm:ml-80 ml-0">
                <div class="w-full relative mt-6" id="chart-work-responden">
                    <div class="w-full">
                        <img src="img/makandirumah-responden.png" alt="" class="w-full">
                    </div>
                </div>
            </div>
        </div>
        <div class="content-wrapper-2 mt-2 pl-6">
            <div class="w-full pb-4 relative">
                <h4 class="text-xl font-black leading-none">54,3 %</h4>
                <p class="content__subs">Responden setelah pandemi lebih memilih membawa bekal untuk makan ketika jam istirahat</p>
                <div class="absolute h-px bg-gray-lighten w-80 inset-0 mt-auto">
                    <div class="absolute left-0 sm:left-full bottom-full top-0 sm:-top-2.5 w-full sm:w-max ml-0 sm:ml-3">Setelah pandemi</div>
                </div>
            </div>

            <div class="content-chart-2 flex sm:ml-80 ml-0">
                <div class="w-full relative mt-6" id="chart-work-responden">
                    <div class="w-full">
                        <img src="img/membawabekal-responden.png" alt="" class="w-full">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- end work responden -->

    <!-- live responden -->

    <section class="container sm:w-2/3 w-auto px-2 sm:px-0 mx-auto pb-6 pt-20" id="live" data-aos="fade-in">
        <div class="content-title-1 border-t-2 border-green w-full relative">
            <span class="absolute top-0 left-0 ml-2">9.</span>
            <span class="bg-green text-white py-0.5 px-2 ml-6 inline-block">Live</span>
        </div>

        <div class="content-wrapper-2 mt-2 pl-6">
            <div class="flex flex-wrap items-start">
                <div class="">
                    <h4 class="text-xl font-black leading-none">79,4 %</h4>
                    <p class="content__subs">Responden membeli barang baru untuk menambah kenyamanan selama di rumah</p>
                </div>

                <div class="content-chart-1 flex items-start">
                    <div class="chart-wrapper relative">
                        <div id="chart-16"></div>
                        <span class="block text-center bottom-0 inset-x-0 text-green">Membeli barang baru</span>
                    </div>
                    <div class="chart-wrapper relative">
                        <div id="chart-17"></div>
                        <span class="block text-center bottom-0 inset-x-0">Tidak membeli barang baru</span>
                    </div>
                </div>
            </div>
            <div class="h-28 relative hidden sm:block">
                <div class="h-44 w-0.5 bg-gray-darkend my-2 absolute bottom-0 left-3"></div>
            </div>
            <div class="flex items-start flex-wrap">
                <div class="">
                    <h4 class="text-xl font-black leading-none">34,4 %</h4>
                    <p class="content__subs">membeli barang elektronik</p>
                    <br>
                    <h4 class="text-xl font-black leading-none">21,1 %</h4>
                    <p class="content__subs">membeli furniture</p>
                </div>

                <div class="content-chart-2">
                    <div class="w-full">
                        <img src="img/barangelektronik-responden.png" alt="" class="w-full">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="container sm:w-2/3 w-auto px-2 sm:px-0 mx-auto py-6" id="live-2" data-aos="fade-in">
        <div class="content-title-1 border-t-2 border-green w-full relative">
            <span class="absolute top-0 left-0">10.</span>
            <span class="bg-green text-white py-0.5 px-2 ml-6 inline-block">Live</span>
        </div>

        <div class="content-wrapper-2 mt-2 pl-6">
            <div class="flex flex-wrap items-start">
                <div class="">
                    <h4 class="text-xl font-black leading-none">80,2 %</h4>
                    <p class="content__subs">Responden memilih membeli barang secara online</p>
                </div>

                <div class="content-chart-1 flex items-start">
                    <div class="chart-wrapper relative">
                        <div id="chart-18"></div>
                        <span class="block text-center bottom-0 inset-x-0 text-green">Belanja online</span>
                    </div>
                </div>
            </div>
            <div class="h-28 relative hidden sm:block">
                <div class="h-44 w-0.5 bg-gray-darkend my-2 absolute bottom-0 left-3"></div>
            </div>
            <div class="flex flex-wrap items-start">
                <div class="">
                    <h4 class="text-xl font-black leading-none">68,1%</h4>
                    <p class="content__subs">Alasan membeli online adalah praktis</p>
                </div>

                <div class="content-chart-2">
                    <div class="w-full">
                        <img src="img/belanjaonline-responden.png" alt="" class="w-full">
                    </div>
                </div>
            </div>
        </div>

        <hr class="w-full opacity-60">

        <div class="content-wrapper-2 mt-2 pl-6">
            <div class="flex flex-wrap items-start">
                <div class="">
                    <h4 class="text-xl font-black leading-none">19,8%</h4>
                    <p class="content__subs">Responden memilih belanja langsung ke toko</p>
                </div>

                <div class="content-chart-1 flex items-start">
                    <div class="chart-wrapper relative">
                        <div id="chart-19"></div>
                        <span class="block text-center bottom-0 inset-x-0">Datang langsung ke toko</span>
                    </div>
                </div>
            </div>
            <div class="h-20 relative hidden sm:block">
                <div class="h-36 w-0.5 bg-gray-darkend my-2 absolute bottom-0 left-3"></div>
            </div>
            <div class="flex flex-wrap items-start">
                <div class="">
                    <h4 class="text-xl font-black leading-none">90,6%</h4>
                    <p class="content__subs">Alasan belanja ke toko adalah lebih suka melihat barang secara langsung</p>
                </div>

                <div class="content-chart-2">
                    <div class="w-full">
                        <img src="img/belanjaditoko-responden.png" alt="" class="w-full">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="container sm:w-2/3 w-auto px-2 sm:px-0 mx-auto py-6" id="live-3" data-aos="fade-in">
        <div class="content-title-1 border-t-2 border-green w-full relative">
            <span class="absolute top-0 left-0">11.</span>
            <span class="bg-green text-white py-0.5 px-2 ml-6 inline-block">Live</span>
        </div>

        <div class="content-wrapper-2 pl-6">
            <div class="w-full pb-4 relative">
                <h4 class="text-xl font-black leading-none">62,3%</h4>
                <p class="content__subs">Responden selama pandemi, membeli kebutuhan sehari-hari dengan pergi kepasar/supermarket dan belanja online</p>
                <div class="absolute h-px bg-gray-lighten w-80 inset-0 mt-auto">
                    <div class="absolute left-0 sm:left-full bottom-full top-0 sm:-top-2.5 w-full sm:w-max ml-0 sm:ml-3">Selama pandemi</div>
                </div>
            </div>

            <div class="content-chart-2 flex mt-2 ml-0 sm:ml-80">
                <div class="w-full relative mt-6">
                    <div class="w-full">
                        <img src="img/belanjakepasar-responden.png" alt="" class="w-full">
                    </div>
                </div>
            </div>
        </div>

        <div class="content-wrapper-3 pl-6">
            <div class="w-full pb-4 relative">
                <h4 class="text-xl font-black leading-none">67,9%</h4>
                <p class="content__subs">Responden setelah pandemi tetap memilih membeli kebutuhan sehari-hari dengan pergi kepasar/supermarket dan belanja online</p>
                <div class="absolute h-px bg-gray-lighten w-80 inset-0 mt-auto">
                    <div class="absolute left-0 sm:left-full bottom-full top-0 sm:-top-2.5 w-full sm:w-max ml-0 sm:ml-3">Setelah pandemi</div>
                </div>
            </div>

            <div class="content-chart-2 flex mt-2 ml-0 sm:ml-80">
                <div class="w-full relative mt-6">
                    <div class="w-full">
                        <img src="img/belanjakepasar-responden-2.png" alt="" class="w-full">
                    </div>
                </div>
            </div>
        </div>

        <div class="content-wrapper-3 pl-6">
            <div class="w-full pb-4 relative">
                <p class="content__subs">Alasan responden memilih jawaban tersebut</p>
                <div class="absolute h-px bg-gray-lighten w-80 inset-0 mt-auto"></div>
            </div>

            <div class="content-chart-2 flex mt-0 sm:-mt-9 ml-0 sm:ml-80 pl-2">
                <div class="w-full relative mt-6 font-semibold">
                    Belanja online
                    <ul class="list-disc mb-2">
                        <li>Responden merasa praktis dan merasa aman tidak berkerumun dengan orang banyak.</li>
                    </ul>

                    Pasar/supermarket
                    <ul class="list-disc mb-2">
                        <li>Responden merasa lebih puas karena melihat langsung dan barang-barang seperti sayuran, daging dan buah lebih
                            suka membeli langsung karena masih segar.</li>
                    </ul>

                    Keduanya
                    <ul class="list-disc">
                        <li>Responden merasa ada barang yang harus di beli langsung dan ada yang bisa via online.</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="container sm:w-2/3 w-auto px-2 sm:px-0 mx-auto py-6" id="live-4" data-aos="fade-in">
        <div class="content-title-1 border-t-2 border-green w-full relative">
            <span class="absolute top-0 left-0">12.</span>
            <span class="bg-green text-white py-0.5 px-2 ml-6 inline-block">Live</span>
        </div>

        <div class="content-wrapper-2 pl-6">
            <div class="w-full pb-4 relative">
                <h4 class="text-xl font-black leading-none">66,2%</h4>
                <p class="content__subs">Responden sebelum pandemi, memasak dan membeli untuk makan sehari-hari</p>
                <div class="absolute h-px bg-gray-lighten w-80 inset-0 mt-auto">
                    <div class="absolute left-0 sm:left-full bottom-full top-0 sm:-top-2.5 w-full sm:w-max ml-0 sm:ml-3">Sebelum pandemi</div>
                </div>
            </div>

            <div class="content-chart-2 flex ml-0 sm:ml-80">
                <div class="w-full relative mt-6">
                    <div class="w-full">
                        <img src="img/memasak-responden.png" alt="" class="w-full">
                    </div>
                </div>
            </div>
        </div>

        <div class="content-wrapper-3 pl-6">
            <div class="w-full pb-4 relative">
                <h4 class="text-xl font-black leading-none">56,9%</h4>
                <p class="content__subs">Responden selama pandemi tetap memasak dan membeli untuk makan sehari-hari</p>
                <div class="absolute h-px bg-gray-lighten w-80 inset-0 mt-auto">
                    <div class="absolute left-0 sm:left-full bottom-full top-0 sm:-top-2.5 w-full sm:w-max ml-0 sm:ml-3">Selama pandemi</div>
                </div>
            </div>

            <div class="content-chart-2 flex ml-0 sm:ml-80">
                <div class="w-full relative mt-6">
                    <div class="w-full">
                        <img src="img/memasak-responden-2.png" alt="" class="w-full">
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- end live responden -->

    <!-- play responden -->

    <section class="container sm:w-2/3 w-auto px-2 sm:px-0 mx-auto pb-6 pt-20" id="play" data-aos="fade-in">
        <div class="content-title-1 border-t-2 border-violet w-full relative">
            <span class="absolute top-0 left-0">13.</span>
            <span class="bg-violet text-white py-0.5 px-2 ml-6 inline-block">Play</span>
        </div>

        <div class="content-wrapper-2 mt-2 pl-6">
            <div class="flex flex-wrap items-start">
                <div class="">
                    <h4 class="text-xl font-black leading-none">83,3%</h4>
                    <p class="content__subs">Responden berolahraga selama pandemi</p>
                </div>

                <div class="content-chart-1 flex items-start">
                    <div class="chart-wrapper relative">
                        <div id="chart-20"></div>
                        <span class="block text-center bottom-0 inset-x-0 text-violet">Berolahraga</span>
                    </div>
                </div>
            </div>
            <div class="h-16 relative hidden sm:block">
                <div class="h-32 w-0.5 bg-gray-darkend my-2 absolute bottom-0 left-3"></div>
            </div>
            <div class="flex flex-wrap items-start">
                <div class="">
                    <h4 class="text-xl font-black leading-none">83,5%</h4>
                    <p class="content__subs">Responden yang berolahraga ingin tetap melanjutkan olahraga setelah pandemi selesai</p>
                </div>

                <div class="content-chart-2">
                    <div class="w-full">
                        <img src="img/inginolahraga-responden.png" alt="" class="w-full">
                    </div>
                </div>
            </div>
        </div>
        <hr class="w-full">
        <div class="content-wrapper-2 mt-2 pl-6">
            <div class="flex flex-wrap items-start">
                <div class="">
                    <h4 class="text-xl font-black leading-none">16,7%</h4>
                    <p class="content__subs">Responden tidak berolahraga selama pandemi</p>
                </div>

                <div class="content-chart-1 flex items-start">
                    <div class="chart-wrapper relative">
                        <div id="chart-21"></div>
                        <span class="block text-center bottom-0 inset-x-0">Tidak berolahraga</span>
                    </div>
                </div>
            </div>
            <div class="h-16 relative hidden sm:block">
                <div class="h-32 w-0.5 bg-gray-darkend my-2 absolute bottom-0 left-3"></div>
            </div>
            <div class="flex flex-wrap items-start">
                <div class="">
                    <h4 class="text-xl font-black leading-none">66,2%</h4>
                    <p class="content__subs">Responden yang tidak berolahraga, menjadi ingin berolahraga setelah pandemi selesai</p>
                </div>

                <div class="content-chart-2">
                    <div class="w-full">
                        <img src="img/inginolahraga-responden-2.png" alt="" class="w-full">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="container sm:w-2/3 w-auto px-2 sm:px-0 mx-auto py-6" id="play-2" data-aos="fade-in">
        <div class="content-title-1 border-t-2 border-violet w-full relative">
            <span class="absolute top-0 left-0">14.</span>
            <span class="bg-violet border-violet text-white py-0.5 px-2 ml-6 inline-block">Play</span>
        </div>

        <div class="content-wrapper-2 pl-6">
            <div class="w-full pb-4 relative">
                <h4 class="text-xl font-black leading-none">45%</h4>
                <p class="content__subs">Tempat olahraga responden berubah menjadi di rumah</p>
                <br>
                <h4 class="text-xl font-black leading-none">22,4%</h4>
                <p class="content__subs">Tempat olahraga responden tidak berubah yaitu tetap di rumah</p>
                <div class="absolute h-px bg-gray-lighten w-80 inset-0 mt-auto">
                    <div class="absolute left-0 sm:left-full bottom-full top-0 sm:-top-2.5 w-full sm:w-max ml-0 sm:ml-3">Selama Pandemi Covid-19, apakah tempat olahraga responden berubah?</div>
                </div>
            </div>

            <div class="content-chart-2 flex mt-4 sm:mt-0 ml-0 sm:ml-80">
                <div class="w-full relative mt-6">
                    <div class="w-full">
                        <img src="img/tempatolahraga.png" alt="" class="w-full">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="container sm:w-2/3 w-auto px-2 sm:px-0 mx-auto py-6" id="play-3" data-aos="fade-in">
        <div class="content-title-1 border-t-2 border-violet w-full relative">
            <span class="absolute top-0 left-0">15.</span>
            <span class="bg-violet text-white py-0.5 px-2 ml-6 inline-block">Play</span>
        </div>

        <div class="content-wrapper-2 mt-2 pl-6">
            <div class="w-full pb-4 relative">
                <h4 class="text-xl font-black leading-none">61,5 %</h4>
                <p class="content__subs">Selama pandemi tempat bersosialisasi responden berubah menjadi bertemu via online</p>
                <div class="absolute h-px bg-gray-lighten w-80 inset-0 mt-auto">
                    <div class="absolute left-0 sm:left-full bottom-full top-0 sm:-top-2.5 w-full sm:w-max ml-0 sm:ml-3">Selama Pandemi, apakah tempat bersosialisasi responden berubah?</div>
                </div>
            </div>

            <div class="content-chart-1 flex mt-8 sm:mt-2 ml-0 sm:ml-80">
                <div class="w-full relative mt-6">
                    <div class="w-full">
                        <img src="img/bertemu-responden.png" alt="" class="w-full">
                    </div>
                </div>
            </div>
        </div>

        <div class="content-wrapper-1 mt-2 pl-6">
            <div class="w-full pb-4 relative">
                <h4 class="text-xl font-black leading-none">44,6%</h4>
                <p class="content__subs">Setelah pandemi, responden berharap bersosialisasi ke café/restoran/pusat perbelanjaan</p>
                <div class="absolute h-px bg-gray-lighten w-80 inset-0 mt-auto">
                    <div class="absolute left-0 sm:left-full bottom-full top-0 sm:-top-2.5 w-full sm:w-max ml-0 sm:ml-3">Setelah pandemi, dimana tempat bersosialisasi yang responden <br> ingin kunjungi?</div>
                </div>
            </div>

            <div class="content-chart-1 flex flex-wrap items-start mt-20 sm:mt-8 ml-0 sm:ml-80">
                <div class="chart-wrapper relative">
                    <div id="chart-22"></div>
                    <span class="block text-center text-violet">Café/restoran/pusat perbelanjaan</span>
                </div>
                <div class="chart-wrapper relative">
                    <div id="chart-23"></div>
                    <span class="block text-center">Di ruang <br>terbuka</span>
                </div>
                <div class="chart-wrapper">
                    <div id="chart-24"></div>
                    <span class="block text-center">Lainnya</span>
                </div>
                <div class="chart-wrapper">
                    <div id="chart-25"></div>
                    <span class="block text-center">Tetap di rumah</span>
                </div>
            </div>
        </div>
    </section>

    <section class="container sm:w-2/3 w-auto px-2 sm:px-0 mx-auto py-6" id="play-4" data-aos="fade-in">
        <div class="content-title-1 border-t-2 border-violet w-full relative">
            <span class="absolute top-0 left-0">16.</span>
            <span class="bg-violet border-violet text-white py-0.5 px-2 ml-6 inline-block">Play</span>
        </div>

        <div class="content-wrapper-2 pl-6">
            <div class="w-full pb-4 relative">
                <h4 class="text-xl font-black leading-none">58,6%</h4>
                <p class="content__subs">Responden memilih menonton sebagai hiburan selama pandemi</p>
                <div class="absolute h-px bg-gray-lighten w-80 inset-0 mt-auto">
                    <div class="absolute left-0 sm:left-full bottom-full top-0 sm:-top-2.5 w-full sm:w-max ml-0 sm:ml-3">Selama pandemi, hiburan apa yang responden pilih?</div>
                </div>
            </div>

            <div class="content-chart-2 flex mt-4 sm:mt-0 ml-0 sm:ml-80">
                <div class="w-full relative mt-6">
                    <div class="w-full">
                        <img src="img/hiburandipilih-responden.png" alt="" class="w-full">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="container sm:w-2/3 w-auto px-2 sm:px-0 mx-auto py-6" id="play-5" data-aos="fade-in">
        <div class="content-title-1 border-t-2 border-violet w-full relative">
            <span class="absolute top-0 left-0">17.</span>
            <span class="bg-violet text-white py-0.5 px-2 ml-6 inline-block">Play</span>
        </div>

        <div class="content-wrapper-1 mt-2 pl-6">
            <div class="w-full pb-4 relative">
                <h4 class="text-xl font-black leading-none">97,3%</h4>
                <p class="content__subs">Responden ingin liburan setelah pandemi selesai</p>
                <div class="absolute h-px bg-gray-lighten w-80 inset-0 mt-auto">
                    <div class="absolute left-0 sm:left-full bottom-full top-0 sm:-top-2.5 w-full sm:w-max ml-0 sm:ml-3">Setelah pandemi, apakah responden ingin liburan?</div>
                </div>
            </div>

            <div class="content-chart-1 flex flex-wrap items-start mt-12 sm:mt-8 ml-0 sm:ml-80">
                <div class="chart-wrapper relative">
                    <div id="chart-26"></div>
                    <span class="block text-center text-violet">Ingin liburan</span>
                </div>
                <div class="chart-wrapper relative">
                    <div id="chart-27"></div>
                    <span class="block text-center">TIdak ingin liburan</span>
                </div>
            </div>
        </div>

        <div class="content-wrapper-2 mt-2 pl-6">
            <div class="w-full pb-4 relative">
                <h4 class="text-xl font-black leading-none">66,8%</h4>
                <p class="content__subs">Responden ingin mengunjungi wisata alam (pantai/taman/hutan kota)</p>
                <div class="absolute h-px bg-gray-lighten w-80 inset-0 mt-auto">
                    <div class="absolute left-0 sm:left-full bottom-full top-0 sm:-top-2.5 w-full sm:w-max ml-0 sm:ml-3">Tempat liburan di kota yang paling ingin responden kunjungi?</div>
                </div>
            </div>

            <div class="content-chart-1 flex mt-2 ml-0 sm:ml-80">
                <div class="w-full relative mt-6">
                    <div class="w-ful">
                        <img src="img/tempatliburan-responden.png" alt="" class="w-full">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- end play section -->

    <!-- kerinduan section -->

    <section class="container sm:w-2/3 w-auto px-2 sm:px-0 mx-auto pb-6 pt-10 sm:pt-20" id="kerinduan" data-aos="fade-in">
        <div class="content-title-1 border-t border-gray-darkend w-full relative">
            <span class="py-1 ml-6 inline-block text-xl font-bold">Kerinduan dan <br>kekhawatiran</span>
        </div>

        <div class="flex flex-wrap sm:flex-nowrap w-full justify-start pl-6">
            <div class="w-full sm:w-80 relative" style="min-width: 20rem;">
                <div class="absolute inset-x-0 top-2 w-full bg-gray-lighten h-px mb-auto"></div>
            </div>

            <div class="ml-2 font-semibold mt-4 sm:mt-0">
                Kegiatan sebelum pandemi yang paling dirindukan:

                <ul class="list-decimal list-inside">
                    <li>Bersosialisasi dengan teman/orang lain tanpa perasaan khawatir</li>
                    <li>Traveling/liburan</li>
                    <li>Nonton konser/bioskop</li>
                </ul>
            </div>
        </div>

        <div class="flex flex-wrap sm:flex-nowrap w-full justify-start pl-6 mt-4">
            <div class="w-full sm:w-80 relative" style="min-width: 20rem;">
                <div class="absolute inset-x-0 top-2 w-full bg-gray-lighten h-px mb-auto"></div>
            </div>

            <div class="ml-2 font-semibold mt-4 sm:mt-0">
                Kekhawatiran terbesar selama pandemi:
                <ul class="list-decimal list-inside">
                    <li>
                        <h5 class="inline">
                            Sisi kesehatan:
                        </h5>
                        <ul class="list-disc list-inside ml-3">
                            <li>
                                Takut tertular/menularkan/menjadi carrier Covid-19
                            </li>
                        </ul>
                    </li>
                    <li>
                        <h5 class="inline">
                            Sisi ekonomi:
                        </h5>
                        <ul class="list-disc list-inside ml-3">
                            <li>Takut kehilangan pekerjaan</li>
                            <li>Pendapatan menurun</li>
                            <li>Sulit mendapatkan pekerjaan baru</li>
                        </ul>
                    </li>
                    <li>
                        <h5 class="inline">
                            Sisi psikologis:
                        </h5>
                        <ul class="list-disc list-inside ml-3">
                            <li>Takut virus tidak akan pernah hilang</li>
                            <li>Ketidakjelasan tentang nasib kegiatan sekolah/kuliah</li>
                            <li>Takut bertemu banyak orang</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </section>

    <!-- end kerinduan section -->

    <section class="container sm:w-2/3 w-auto px-2 sm:px-0 mx-auto pb-6 pt-20" id="kesimpulan" data-aos="fade-in">
        <div class="content-title-1 border-t border-gray-darkend w-full relative">
            <span class="py-1 ml-6 inline-block text-xl font-bold">Kesimpulan</span>
        </div>

        <div class="flex flex-wrap sm:flex-nowrap w-full justify-around pl-6">
            <div class="w-full sm:w-80 relative" style="min-width: 20rem;">
                <div class="w-full bg-gray-lighten h-px mt-2 mb-auto"></div>
            </div>

            <div class="ml-2 font-semibold w-auto">
                <span class="text-pink inline-block">Work</span>
                <ul class="list-disc ml-3">
                    <li class="break-words"><span>Responden (42,3%) merasa bekerja di rumah seefektif bekerja di kantor, namun tetap berharap bisa bekerja kombinasi antara di rumah dan di kantor (55,6%).</span></li>
                    <li class="break-words"><span>Tren membawa bekal untuk makan siang akan semakin naik, namun memasak/makan di rumah akan menurun.</span></li>
                    <li class="break-words"><span>Banyak orang (60,6%) mengharapkan dapat kembali meeting secara tatap muka. Meeting online tetap jadi pilihan, tapi hanya untuk bagi (39,4%).</span></li>
                </ul>
            </div>
        </div>


        <div class="flex flex-wrap sm:flex-nowrap w-full justify-start pl-6">
            <div class="w-full sm:w-80 relative" style="min-width: 20rem;">
                <div class="w-full bg-gray-lighten h-px mt-2 mb-auto"></div>
            </div>

            <div class="ml-2 font-semibold">
                <span class="text-green inline-block">Live</span>
                <ul class="list-disc ml-3">
                    <li class="break-words"><span>Barang elektronik (34,4%) adalah barang yang banyak di beli oleh responden selama pandemi.</span></li>
                    <li class="break-words"><span>Banyak orang (80,2%) lebih suka berbelanja online karena praktis. Datang langsung ke toko hanya pilihan dari (19,8%) karena lebih suka melihat barang secara langsung.</span></li>
                    <li class="break-words"><span>Tren memasak untuk makan sehari-hari meningkat selama pandemi.</span></li>
                </ul>
            </div>
        </div>

        <div class="flex flex-wrap sm:flex-nowrap w-full justify-start pl-6">
            <div class="w-full sm:w-80 relative" style="min-width: 20rem;">
                <div class="w-full bg-gray-lighten h-px mt-2 mb-auto"></div>
            </div>

            <div class="ml-2 font-semibold">
                <span class="text-violet inline-block">Play</span>
                <ul class="list-disc ml-3">
                    <li class="break-words"><span>Pandemi membuat responden berolahraga di rumah.</span></li>
                    <li class="break-words"><span>Bersosialisasi via online menjadi pilihan responden selama pandemi, namun responden tetap mengharapkan bersosialisasi di café/restoran/pusat perbelanjaan.</span></li>
                    <li class="break-words"><span>Tempat liburan pilihan responden ketika pandemi berakhir adalah wisata alam seperti pantai/taman/hutan kota (66,8%).</span></li>
                </ul>
            </div>
        </div>
    </section>

    <!-- saran section -->

    <section class="container sm:w-2/3 w-auto px-2 sm:px-0 mx-auto py-6" id="saran" data-aos="fade-in">
        <div class="content-title-1 border-t border-gray-darkend w-full relative">
            <span class="py-1 ml-6 inline-block text-xl font-bold">Saran</span>
        </div>

        <div class="flex flex-wrap sm:flex-nowrap w-full justify-around pl-6">
            <div class="w-full sm:w-80 relative" style="min-width: 20rem;">
                <div class="w-full bg-gray-lighten h-px mt-2 mb-auto"></div>
            </div>

            <div class="ml-2 font-semibold w-auto">
                <ul class="list-disc ml-3">
                    <li class="break-words"><span>Pemerintah memberikan fleksibilitas perubahan fungsi ruang untuk gedung perkantoran di Jakarta. Okupansi gedung
                            perkantoran kemungkinan akan menurun karena adanya keinginan kombinasi kantor dan rumah sebagai tempat bekerja.
                            <br>
                            Fungsi kantor akan berubah menjadi lebih banyak ruang meeting dan ruang bersosialisasi/team building. Fleksibilitas penggunaan
                            ruang memungkinkan adanya kesempatan penciptaan lapangan kerja yang baru.</span></li>
                    <li class="break-words"><span>Pemerintah merevitalisasi pasar yang sudah ada untuk menambah kenyamanan masyarakat. Sebab di era yang serba digital ini
                            masyarakat tetap memilih belanja di pasar untuk
                            mencukupi kebutuhan</span></li>
                    <li class="break-words"><span>Pemerintah menambah hutan kota/daerah terbuka hijau yang bisa digunakan oleh masyarakat untuk berekreasi. Sebab pola
                            masyarakat sudah berubah, sudah tidak ingin pergi ke tempat belanja melainkan kembali ke alam terbuka.</span></li>
                </ul>
            </div>
        </div>
    </section>

    <div class="flex justify-center items-center">
        <a href="https://jpi.or.id/" class="rounded-full p-2 text-white bg-gray-darkend hover:bg-black transition-colors duration-700 flex items-center">Kembali ke website JPI</a>
    </div>

    <a class="bg-gray-darkend hover:bg-black transition-colors duration-700" id="button"></a>

    <script src="dist/main.js"></script>

    <script>
        var btn = $('#button');

        $(window).scroll(function() {
            if ($(window).scrollTop() > 300) {
                btn.addClass('show');
            } else {
                btn.removeClass('show');
            }
        });

        btn.on('click', function(e) {
            e.preventDefault();
            $('html, body').animate({
                scrollTop: 0
            }, '300');
        });

        $(document).ready(function() {
            var heroHeight = $('.hero').height();
            $(window).bind('scroll', function() {
                if ($(window).scrollTop() > heroHeight) {
                    $('.navigation').addClass('nav-fixed shadow-md');
                    $('.navigation-mobile').css({
                        'backgroundColor': '#fff'
                    });
                } else {
                    $('.navigation').removeClass('nav-fixed shadow-md');
                    $('.navigation-mobile').css({
                        'backgroundColor': 'transparent'
                    });
                }
            });
            if ($(window).scrollTop() > heroHeight) {
                $('.navigation').addClass('nav-fixed');
            } else {
                $('.navigation').removeClass('nav-fixed');
            }

            $('.navigation-mobile #menu-toggle').on('click', function(e) {
                e.preventDefault();
                $('.navigation-mobile').toggleClass('show')
            })

            $('.navigation-mobile ul li').on('click', function() {
                $('.navigation-mobile').toggleClass('show')
            })
        });
    </script>
</body>

</html>