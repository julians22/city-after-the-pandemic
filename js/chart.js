const defaultColor = "#32444B",
    defaultFontSize = "14px",
    blueColor = "#629DAC",
    greenColor = "#9BBA61",
    violetColor = "#80659F",
    pinkColor = "#CC7272";

    const grid = {
      padding: {
        top: -20,
        bottom: -15,
        left: -20,
        right: -20
      },
    }

    const chartConf = {
      height: 120,
      width: 120,
      type: "radialBar",
    }

const karyawanChart = new ApexCharts(document.querySelector("#chart-1"), {
    chart: chartConf,
    grid: grid,
    series: [56.4],
    colors: [blueColor],
    plotOptions: {
        radialBar: {
            hollow: {
                margin: 0,
                size: "50%",
            },

            track: {
                show: true,
                strokeWidth: 2,
                background: "#000",
            },

            dataLabels: {
                name: {
                    show: false,
                },
                value: {
                    fontSize: "14px",
                    show: true,
                    offsetY: 5,
                    fontFamily: "circular",
                    fontWeight: 800,
                    color: blueColor,
                },
            },
        },
    },

    stroke: {
        lineCap: "butt",
    },
    labels: ["Karyawan"],
});

const pelajarChart = new ApexCharts(document.querySelector("#chart-2"), {
  chart: chartConf,
  grid: grid,
  series: [22.8],
  colors: [defaultColor],
  plotOptions: {
    radialBar: {
      hollow: {
        margin: 0,
        size: "50%",
      },

      track: {
        show: true,
        strokeWidth: 2,
        background: "#000",
      },

      dataLabels: {
        name: {
          show: false,
        },
        value: {
          fontSize: defaultFontSize,
          show: true,
          offsetY: 5,
          fontFamily: "circular",
          fontWeight: 800,
          color: defaultColor,
        },
      },
    },
  },

  stroke: {
    lineCap: "butt",
  },
  labels: ["Pelajar/Mahasiswa"],
});

const pekerjaLepasChart = new ApexCharts(document.querySelector("#chart-3"), {
  chart: chartConf,
  grid: grid,
  series: [10.8],
  colors: [defaultColor],
  plotOptions: {
    radialBar: {
      hollow: {
        margin: 0,
        size: "50%",
      },

      track: {
        show: true,
        strokeWidth: 2,
        background: "#000",
      },

      dataLabels: {
        name: {
          show: false,
        },
        value: {
          fontSize: defaultFontSize,
          show: true,
          offsetY: 5,
          fontFamily: "circular",
          fontWeight: 800,
          color: defaultColor,
        },
      },
    },
  },

  stroke: {
    lineCap: "butt",
  },
  labels: ["Pekerja Lepas"],
});

const wiraswastaChart = new ApexCharts(document.querySelector("#chart-4"), {
  chart: chartConf,
  grid: grid,
  series: [10],
  colors: [defaultColor],
  plotOptions: {
    radialBar: {
      hollow: {
        margin: 0,
        size: "50%",
      },

      track: {
        show: true,
        strokeWidth: 2,
        background: "#000",
      },

      dataLabels: {
        name: {
          show: false,
        },
        value: {
          fontSize: defaultFontSize,
          show: true,
          offsetY: 5,
          fontFamily: "circular",
          fontWeight: 800,
          color: defaultColor,
        },
      },
    },
  },

  stroke: {
    lineCap: "butt",
  },
  labels: ["Wiraswasta"],
});

// work section chart

const dkiChart = new ApexCharts(document.querySelector("#chart-5"), {
  chart: chartConf,
  grid: grid,
  series: [65.4],
  colors: [pinkColor],
  plotOptions: {
    radialBar: {
      hollow: {
        margin: 0,
        size: "50%",
      },

      track: {
        show: true,
        strokeWidth: 2,
        background: "#000",
      },

      dataLabels: {
        name: {
          show: false,
        },
        value: {
          fontSize: "14px",
          show: true,
          offsetY: 5,
          fontFamily: "circular",
          fontWeight: 800,
          color: pinkColor,
        },
      },
    },
  },

  stroke: {
    lineCap: "butt",
  },
  labels: ["DKI Jakarta"],
});

const bodetabekChart = new ApexCharts(document.querySelector("#chart-6"), {
  chart: chartConf,
  grid: grid,
  series: [21.6],
  colors: [defaultColor],
  plotOptions: {
    radialBar: {
      hollow: {
        margin: 0,
        size: "50%",
      },

      track: {
        show: true,
        strokeWidth: 2,
        background: "#000",
      },

      dataLabels: {
        name: {
          show: false,
        },
        value: {
          fontSize: defaultFontSize,
          show: true,
          offsetY: 5,
          fontFamily: "circular",
          fontWeight: 800,
          color: defaultColor,
        },
      },
    },
  },

  stroke: {
    lineCap: "butt",
  },
  labels: ["Bodetabek"],
});

const luarjabodetabekChart = new ApexCharts(
  document.querySelector("#chart-7"),
  {
    chart: chartConf,
    grid: grid,
    series: [13],
    colors: [defaultColor],
    plotOptions: {
      radialBar: {
        hollow: {
          margin: 0,
          size: "50%",
        },

        track: {
          show: true,
          strokeWidth: 2,
          background: "#000",
        },

        dataLabels: {
          name: {
            show: false,
          },
          value: {
            fontSize: defaultFontSize,
            show: true,
            offsetY: 5,
            fontFamily: "circular",
            fontWeight: 800,
            color: defaultColor,
          },
        },
      },
    },

    stroke: {
      lineCap: "butt",
    },
    labels: ["Luar Jabodetabek"],
  }
);

const kombinasiRumahChart = new ApexCharts(document.querySelector("#chart-8"), {
    chart: chartConf,
  grid: grid,
  series: [55.6],
  colors: [pinkColor],
  plotOptions: {
    radialBar: {
      hollow: {
        margin: 0,
        size: "50%",
      },

      track: {
        show: true,
        strokeWidth: 2,
        background: "#000",
      },

      dataLabels: {
        name: {
          show: false,
        },
        value: {
          fontSize: defaultFontSize,
          show: true,
          offsetY: 5,
          fontFamily: "circular",
          fontWeight: 800,
          color: pinkColor,
        },
      },
    },
  },

  stroke: {
    lineCap: "butt",
  },
  labels: ["Kombinasi rumah"],
});

const totalKantorChart = new ApexCharts(document.querySelector("#chart-9"), {
    chart: chartConf,
  grid: grid,
  series: [28.9],
  colors: [defaultColor],
  plotOptions: {
    radialBar: {
      hollow: {
        margin: 0,
        size: "50%",
      },

      track: {
        show: true,
        strokeWidth: 2,
        background: "#000",
      },

      dataLabels: {
        name: {
          show: false,
        },
        value: {
          fontSize: defaultFontSize,
          show: true,
          offsetY: 5,
          fontFamily: "circular",
          fontWeight: 800,
          color: defaultColor,
        },
      },
    },
  },

  stroke: {
    lineCap: "butt",
  },
  labels: ["Total di kantor"],
});

const totalRumahChart = new ApexCharts(document.querySelector("#chart-10"), {
    chart: chartConf,
  grid: grid,
  series: [8.6],
  colors: [defaultColor],
  plotOptions: {
    radialBar: {
      hollow: {
        margin: 0,
        size: "50%",
      },

      track: {
        show: true,
        strokeWidth: 2,
        background: "#000",
      },

      dataLabels: {
        name: {
          show: false,
        },
        value: {
          fontSize: defaultFontSize,
          show: true,
          offsetY: 5,
          fontFamily: "circular",
          fontWeight: 800,
          color: defaultColor,
        },
      },
    },
  },

  stroke: {
    lineCap: "butt",
  },
  labels: ["Total di rumah"],
});

const tetapTokoRukoChart = new ApexCharts(document.querySelector("#chart-11"), {
    chart: chartConf,
  grid: grid,
  series: [7],
  colors: [defaultColor],
  plotOptions: {
    radialBar: {
      hollow: {
        margin: 0,
        size: "50%",
      },

      track: {
        show: true,
        strokeWidth: 2,
        background: "#000",
      },

      dataLabels: {
        name: {
          show: false,
        },
        value: {
          fontSize: defaultFontSize,
          show: true,
          offsetY: 5,
          fontFamily: "circular",
          fontWeight: 800,
          color: defaultColor,
        },
      },
    },
  },

  stroke: {
    lineCap: "butt",
  },
  labels: ["Tetap di toko/ruko/tempat kerja karena tidak ada pilihan"],
});

const transPribadiChart = new ApexCharts(document.querySelector("#chart-12"), {
    chart: chartConf,
  grid: grid,
  series: [69],
  colors: [pinkColor],
  plotOptions: {
    radialBar: {
      hollow: {
        margin: 0,
        size: "50%",
      },

      track: {
        show: true,
        strokeWidth: 2,
        background: "#000",
      },

      dataLabels: {
        name: {
          show: false,
        },
        value: {
          fontSize: defaultFontSize,
          show: true,
          offsetY: 5,
          fontFamily: "circular",
          fontWeight: 800,
          color: pinkColor,
        },
      },
    },
  },

  stroke: {
    lineCap: "butt",
  },
  labels: ["Transportasi pribadi"],
});

const transUmumChart = new ApexCharts(document.querySelector("#chart-13"), {
    chart: chartConf,
  grid: grid,
  series: [18],
  colors: [defaultColor],
  plotOptions: {
    radialBar: {
      hollow: {
        margin: 0,
        size: "50%",
      },

      track: {
        show: true,
        strokeWidth: 2,
        background: "#000",
      },

      dataLabels: {
        name: {
          show: false,
        },
        value: {
          fontSize: defaultFontSize,
          show: true,
          offsetY: 5,
          fontFamily: "circular",
          fontWeight: 800,
          color: defaultColor,
        },
      },
    },
  },

  stroke: {
    lineCap: "butt",
  },
  labels: ["Transportasi umum"],
});

const transOnlineChart = new ApexCharts(document.querySelector("#chart-14"), {
    chart: chartConf,
  grid: grid,
  series: [9.5],
  colors: [defaultColor],
  plotOptions: {
    radialBar: {
      hollow: {
        margin: 0,
        size: "50%",
      },

      track: {
        show: true,
        strokeWidth: 2,
        background: "#000",
      },

      dataLabels: {
        name: {
          show: false,
        },
        value: {
          fontSize: defaultFontSize,
          show: true,
          offsetY: 5,
          fontFamily: "circular",
          fontWeight: 800,
          color: defaultColor,
        },
      },
    },
  },

  stroke: {
    lineCap: "butt",
  },
  labels: ["Tetap di toko/ruko/tempat kerja karena tidak ada pilihan"],
});

const jalanKakiChart = new ApexCharts(document.querySelector("#chart-15"), {
    chart: chartConf,
  grid: grid,
  series: [3.5],
  colors: [defaultColor],
  plotOptions: {
    radialBar: {
      hollow: {
        margin: 0,
        size: "50%",
      },

      track: {
        show: true,
        strokeWidth: 2,
        background: "#000",
      },

      dataLabels: {
        name: {
          show: false,
        },
        value: {
          fontSize: defaultFontSize,
          show: true,
          offsetY: 5,
          fontFamily: "circular",
          fontWeight: 800,
          color: defaultColor,
        },
      },
    },
  },

  stroke: {
    lineCap: "butt",
  },
  labels: ["Tetap di toko/ruko/tempat kerja karena tidak ada pilihan"],
});


// live section chart

const membeliBarangChart = new ApexCharts(document.querySelector("#chart-16"), {
    chart: chartConf,
  grid: grid,
  series: [79.4],
  colors: [greenColor],
  plotOptions: {
    radialBar: {
      hollow: {
        margin: 0,
        size: "50%",
      },

      track: {
        show: true,
        strokeWidth: 2,
        background: "#000",
      },

      dataLabels: {
        name: {
          show: false,
        },
        value: {
          fontSize: "14px",
          show: true,
          offsetY: 5,
          fontFamily: "circular",
          fontWeight: 800,
          color: greenColor,
        },
      },
    },
  },

  stroke: {
    lineCap: "butt",
  },
  labels: ["Memberli barang baru"],
});

const tidakMemberliBarang = new ApexCharts(
  document.querySelector("#chart-17"),
  {
    chart: chartConf,
    grid: grid,
    series: [20.6],
    colors: [defaultColor],
    plotOptions: {
      radialBar: {
        hollow: {
          margin: 0,
          size: "50%",
        },

        track: {
          show: true,
          strokeWidth: 2,
          background: "#000",
        },

        dataLabels: {
          name: {
            show: false,
          },
          value: {
            fontSize: defaultFontSize,
            show: true,
            offsetY: 5,
            fontFamily: "circular",
            fontWeight: 800,
            color: defaultColor,
          },
        },
      },
    },

    stroke: {
      lineCap: "butt",
    },
    labels: ["Tetap di toko/ruko/tempat kerja karena tidak ada pilihan"],
  }
);

const belanjaOnlineChart = new ApexCharts(
  document.querySelector("#chart-18"),
  {
    chart: chartConf,
    grid: grid,
    series: [80.2],
    colors: [greenColor],
    plotOptions: {
      radialBar: {
        hollow: {
          margin: 0,
          size: "50%",
        },

        track: {
          show: true,
          strokeWidth: 2,
          background: "#000",
        },

        dataLabels: {
          name: {
            show: false,
          },
          value: {
            fontSize: defaultFontSize,
            show: true,
            offsetY: 5,
            fontFamily: "circular",
            fontWeight: 800,
            color: greenColor,
          },
        },
      },
    },

    stroke: {
      lineCap: "butt",
    },
    labels: ["Belanja online"],
  }
);

const belanjaDiToko = new ApexCharts(
  document.querySelector("#chart-19"),
  {
    chart: chartConf,
    grid: grid,
    series: [19.2],
    colors: [defaultColor],
    plotOptions: {
      radialBar: {
        hollow: {
          margin: 0,
          size: "50%",
        },

        track: {
          show: true,
          strokeWidth: 2,
          background: "#000",
        },

        dataLabels: {
          name: {
            show: false,
          },
          value: {
            fontSize: defaultFontSize,
            show: true,
            offsetY: 5,
            fontFamily: "circular",
            fontWeight: 800,
            color: defaultColor,
          },
        },
      },
    },

    stroke: {
      lineCap: "butt",
    },
    labels: ["Datang langsung ke toko"],
  }
);

// play chart

const olahragaChart = new ApexCharts(document.querySelector("#chart-20"), {
  chart: chartConf,
  grid: grid,
  series: [83.3],
  colors: [violetColor],
  plotOptions: {
    radialBar: {
      hollow: {
        margin: 0,
        size: "50%",
      },

      track: {
        show: true,
        strokeWidth: 2,
        background: "#000",
      },

      dataLabels: {
        name: {
          show: false,
        },
        value: {
          fontSize: defaultFontSize,
          show: true,
          offsetY: 5,
          fontFamily: "circular",
          fontWeight: 800,
          color: violetColor,
        },
      },
    },
  },

  stroke: {
    lineCap: "butt",
  },
  labels: ["olahraga"],
});

const tidakOlahragaChart = new ApexCharts(document.querySelector("#chart-21"), {
  chart: chartConf,
  grid: grid,
  series: [16.7],
  colors: [defaultColor],
  plotOptions: {
    radialBar: {
      hollow: {
        margin: 0,
        size: "50%",
      },

      track: {
        show: true,
        strokeWidth: 2,
        background: "#000",
      },

      dataLabels: {
        name: {
          show: false,
        },
        value: {
          fontSize: defaultFontSize,
          show: true,
          offsetY: 5,
          fontFamily: "circular",
          fontWeight: 800,
          color: defaultColor,
        },
      },
    },
  },

  stroke: {
    lineCap: "butt",
  },
  labels: ["tidak olahraga"],
});

const cafeChart = new ApexCharts(document.querySelector("#chart-22"), {
  chart: chartConf,
  grid: grid,
  series: [44.6],
  colors: [violetColor],
  plotOptions: {
    radialBar: {
      hollow: {
        margin: 0,
        size: "50%",
      },

      track: {
        show: true,
        strokeWidth: 2,
        background: "#000",
      },

      dataLabels: {
        name: {
          show: false,
        },
        value: {
          fontSize: defaultFontSize,
          show: true,
          offsetY: 5,
          fontFamily: "circular",
          fontWeight: 800,
          color: violetColor,
        },
      },
    },
  },

  stroke: {
    lineCap: "butt",
  },
  labels: ["cafe/restoran/pusat perbelanjaan"],
});

const ruangTerbukaChart = new ApexCharts(document.querySelector("#chart-23"), {
  chart: chartConf,
  grid: grid,
  series: [42.6],
  colors: [defaultColor],
  plotOptions: {
    radialBar: {
      hollow: {
        margin: 0,
        size: "50%",
      },

      track: {
        show: true,
        strokeWidth: 2,
        background: "#000",
      },

      dataLabels: {
        name: {
          show: false,
        },
        value: {
          fontSize: defaultFontSize,
          show: true,
          offsetY: 5,
          fontFamily: "circular",
          fontWeight: 800,
          color: defaultColor,
        },
      },
    },
  },

  stroke: {
    lineCap: "butt",
  },
  labels: ["Dirumah"],
});

const lainnyaChart = new ApexCharts(document.querySelector("#chart-24"), {
  chart: chartConf,
  grid: grid,
  series: [5.6],
  colors: [defaultColor],
  plotOptions: {
    radialBar: {
      hollow: {
        margin: 0,
        size: "50%",
      },

      track: {
        show: true,
        strokeWidth: 2,
        background: "#000",
      },

      dataLabels: {
        name: {
          show: false,
        },
        value: {
          fontSize: defaultFontSize,
          show: true,
          offsetY: 5,
          fontFamily: "circular",
          fontWeight: 800,
          color: defaultColor,
        },
      },
    },
  },

  stroke: {
    lineCap: "butt",
  },
  labels: ["Lainnya"],
});

const teteapDirumahChart = new ApexCharts(document.querySelector("#chart-25"), {
  chart: chartConf,
  grid: grid,
  series: [5.1],
  colors: [defaultColor],
  plotOptions: {
    radialBar: {
      hollow: {
        margin: 0,
        size: "50%",
      },

      track: {
        show: true,
        strokeWidth: 2,
        background: "#000",
      },

      dataLabels: {
        name: {
          show: false,
        },
        value: {
          fontSize: defaultFontSize,
          show: true,
          offsetY: 5,
          fontFamily: "circular",
          fontWeight: 800,
          color: defaultColor,
        },
      },
    },
  },

  stroke: {
    lineCap: "butt",
  },
  labels: ["Teteap dirumah"],
});

const inginLiburanChart = new ApexCharts(document.querySelector("#chart-26"), {
  chart: chartConf,
  grid: grid,
  series: [97.3],
  colors: [violetColor],
  plotOptions: {
    radialBar: {
      hollow: {
        margin: 0,
        size: "50%",
      },

      track: {
        show: true,
        strokeWidth: 2,
        background: "#000",
      },

      dataLabels: {
        name: {
          show: false,
        },
        value: {
          fontSize: defaultFontSize,
          show: true,
          offsetY: 5,
          fontFamily: "circular",
          fontWeight: 800,
          color: violetColor,
        },
      },
    },
  },

  stroke: {
    lineCap: "butt",
  },
  labels: ["Ingin liburan"],
});

const tidakInginLiburanChart = new ApexCharts(document.querySelector("#chart-27"), {
  chart: chartConf,
  grid: grid,
  series: [2.7],
  colors: [defaultColor],
  plotOptions: {
    radialBar: {
      hollow: {
        margin: 0,
        size: "50%",
      },

      track: {
        show: true,
        strokeWidth: 2,
        background: "#000",
      },

      dataLabels: {
        name: {
          show: false,
        },
        value: {
          fontSize: defaultFontSize,
          show: true,
          offsetY: 5,
          fontFamily: "circular",
          fontWeight: 800,
          color: defaultColor,
        },
      },
    },
  },

  stroke: {
    lineCap: "butt",
  },
  labels: ["Tidak ingin liburan"],
});

karyawanChart.render();
pelajarChart.render();
pekerjaLepasChart.render();
wiraswastaChart.render();
dkiChart.render();
bodetabekChart.render();
luarjabodetabekChart.render();
kombinasiRumahChart.render();
totalKantorChart.render();
totalRumahChart.render();
tetapTokoRukoChart.render();
transPribadiChart.render();
transUmumChart.render();
transOnlineChart.render();
jalanKakiChart.render();
membeliBarangChart.render();
tidakMemberliBarang.render();
belanjaOnlineChart.render();
belanjaDiToko.render();
olahragaChart.render();
tidakOlahragaChart.render();
cafeChart.render();
ruangTerbukaChart.render();
lainnyaChart.render();
teteapDirumahChart.render();
inginLiburanChart.render();
tidakInginLiburanChart.render();