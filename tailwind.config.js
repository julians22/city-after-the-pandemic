const colors = require("tailwindcss/colors");

module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      blue: "#629DAC",
      pink: "#CC7272",
      green: "#9BBA61",
      violet: "#80659F",
      gray: {
        lighten: "#939598",
        darkend: "#32444B",
      },
      white: colors.white,
      black: colors.black,
    },
    extend: {
      fontFamily: {
        circular: ["Circular", "ui-sans-serif", "system-ui"],
      },
      height: {
        "80-vh": "80vh",
        "85-vh": "85vh",
        "90-vh": "90vh",
      },
      inset: {
        62: "16rem",
      },
      maxWidth: {
        "200px": "200px",
        "210px": "210px",
      },
      minWidth: {
        "150px": "150px",
        "165px": "165px",
      },
    },
  },
  variants: {
    extend: {
      display: ['group-hover']
    },
  },
  plugins: [],
};
